package com.leafBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.leafBot.pages.LoginPage;
import com.leafBot.pages.MyHomePage;
import com.leafBot.testng.api.base.Annotations;

public class CreateLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create a lead";
		author = "joyce";
		category = "regression";
		excelFileName = "TC001";
	} 
	
	@Test(dataProvider="fetchData")
	public void createLeadTest(String cName, String fName, String lName) {
	
		
		new MyHomePage()
		.clickLeadsTab()
		.clickCreateLead()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCreateLeadButton()
		.verifyLeadIsCreated();
	}

}